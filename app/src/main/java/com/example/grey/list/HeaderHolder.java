package com.example.grey.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class HeaderHolder extends RecyclerView.ViewHolder {
    public TextView headerText;

    private HeaderHolder(View itemView) {
        super(itemView);

        headerText = (TextView) itemView;
    }

    public static HeaderHolder createHolder(ViewGroup parent) {
        return new HeaderHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.section_header, parent, false));
    }
}
