package com.example.grey.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.serega.fastscroller.FastScrollAdapter;
import com.serega.fastscroller.FastScroller;
import com.serega.stickyheaders.BaseSectionsAdapter;
import com.serega.stickyheaders.StickyHeaderAdapter;
import com.serega.stickyheaders.StickyHeaderDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_list);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv.setVerticalScrollBarEnabled(false);
        Adapter adapter = new Adapter(NameGenerator.generateNames());
        rv.setAdapter(adapter);
        rv.setVerticalScrollbarPosition(RecyclerView.SCROLLBAR_POSITION_RIGHT);

        rv.addItemDecoration(new StickyHeaderDecoration());

        FastScroller.wrap(rv);
    }

    class Adapter extends BaseSectionsAdapter implements
            StickyHeaderAdapter<HeaderHolder>,
            FastScrollAdapter {
        private final Map<String, List<String>> names;
        private final List<String> sectionLetters;

        Adapter(Map<String, List<String>> names) {
            this.names = names;
            sectionLetters = new ArrayList<>();
            sectionLetters.addAll(names.keySet());
            Collections.sort(sectionLetters);
        }

        @Override
        public String getLetter(int position) {
            int section = getSectionForPosition(position);
            if (section == -1) {
                section = sectionLetters.size() - 1;
            }
            return sectionLetters.get(section);
        }

        @Override
        public int getSectionCount() {
            return sectionLetters.size();
        }

        @Override
        public int getCountForSection(int section) {
            return names.get(sectionLetters.get(section)).size();
        }

        @Override
        public int getItemViewType(int section, int position) {
            return R.layout.holder;
        }

        @Override
        public void onBindViewHolder(int section, int position, RecyclerView.ViewHolder holder) {
            String key = sectionLetters.get(section);
            String name = names.get(key).get(position);
            ((ItemHolder) holder).text.setText(name);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return ItemHolder.createHolder(viewGroup);
        }

        @Override
        public long getHeaderId(int position) {
            return sectionLetters.get(getSectionForPosition(position)).hashCode();
        }

        @Override
        public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
            return HeaderHolder.createHolder(parent);
        }

        @Override
        public void onBindHeaderViewHolder(HeaderHolder holder, int position) {
            holder.headerText.setText(sectionLetters.get(getSectionForPosition(position)));
        }

        @Override
        public int getPositionForScrollProgress(float progress) {
            return (int) ((getItemCount() - 1) * progress);
        }
    }
}
