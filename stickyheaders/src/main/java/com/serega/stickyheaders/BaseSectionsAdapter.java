package com.serega.stickyheaders;

import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public abstract class BaseSectionsAdapter extends RecyclerView.Adapter {
    private static final int NO_VALUE = -1;
    private final SparseIntArray sectionPositionCache = new SparseIntArray();
    private final SparseIntArray sectionCache = new SparseIntArray();
    private final SparseIntArray sectionCountCache = new SparseIntArray();
    private int sectionCount = NO_VALUE;
    private int count = NO_VALUE;

    protected BaseSectionsAdapter() {
        super();

        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                cleanupCache();
                super.onChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (count >= 0) {
            return count;
        }
        count = 0;
        for (int i = 0, size = internalGetSectionCount(); i < size; ++i) {
            count += internalGetCountForSection(i);
        }
        return count;
    }

    public final int getItemViewType(int position) {
        return getItemViewType(getSectionForPosition(position), getPositionInSectionForPosition(position));
    }

    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindViewHolder(getSectionForPosition(position), getPositionInSectionForPosition(position), holder);
    }

    private int internalGetCountForSection(int section) {
        int cachedSectionCount = sectionCountCache.get(section, NO_VALUE);
        if (cachedSectionCount != NO_VALUE) {
            return cachedSectionCount;
        }
        int sectionCount = getCountForSection(section);
        sectionCountCache.put(section, sectionCount);
        return sectionCount;
    }

    private int internalGetSectionCount() {
        if (sectionCount >= 0) {
            return sectionCount;
        }
        sectionCount = getSectionCount();
        return sectionCount;
    }

    protected final int getSectionForPosition(int position) {
        int cachedSection = sectionCache.get(position, NO_VALUE);
        if (cachedSection != NO_VALUE) {
            return cachedSection;
        }
        int sectionStart = 0;
        for (int i = 0, size = internalGetSectionCount(); i < size; ++i) {
            int sectionCount = internalGetCountForSection(i);
            int sectionEnd = sectionStart + sectionCount;
            if (position >= sectionStart && position < sectionEnd) {
                sectionCache.put(position, i);
                return i;
            }
            sectionStart = sectionEnd;
        }
        return NO_VALUE;
    }

    protected int getPositionInSectionForPosition(int position) {
        int cachedPosition = sectionPositionCache.get(position, NO_VALUE);
        if (cachedPosition != NO_VALUE) {
            return cachedPosition;
        }
        int sectionStart = 0;
        for (int i = 0, size = internalGetSectionCount(); i < size; ++i) {
            int sectionCount = internalGetCountForSection(i);
            int sectionEnd = sectionStart + sectionCount;
            if (position >= sectionStart && position < sectionEnd) {
                int positionInSection = position - sectionStart;
                sectionPositionCache.put(position, positionInSection);
                return positionInSection;
            }
            sectionStart = sectionEnd;
        }
        return NO_VALUE;
    }

    public abstract int getSectionCount();

    public abstract int getCountForSection(int section);

    public abstract int getItemViewType(int section, int position);

    public abstract void onBindViewHolder(int section, int position, RecyclerView.ViewHolder holder);

    private void cleanupCache() {
        sectionCache.clear();
        sectionPositionCache.clear();
        sectionCountCache.clear();
        count = -1;
        sectionCount = -1;
    }
}
