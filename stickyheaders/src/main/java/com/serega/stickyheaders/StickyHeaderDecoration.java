/*
 * Copyright 2014 Eduardo Barrenechea
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.serega.stickyheaders;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.View;
import android.view.ViewGroup;

/**
 * A sticky header decoration for android's RecyclerView.
 */
public class StickyHeaderDecoration extends RecyclerView.ItemDecoration {
    private final LongSparseArray<RecyclerView.ViewHolder> headerCache = new LongSparseArray<>();
    private static final long NO_ID = -1L;

    /**
     * {@inheritDoc}
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        RecyclerView.ViewHolder header = getHeader(parent, parent.getChildAdapterPosition(view));
        if (header != null) {
            outRect.set(header.itemView.getMeasuredWidth(), 0, 0, 0);
        }
    }

    private boolean hasHeader(@NonNull StickyHeaderAdapter adapter, int position) {
        return adapter.getHeaderId(position) != NO_ID;
    }

    @Nullable
    private RecyclerView.ViewHolder getHeader(RecyclerView parent, int position) {
        if (parent.getAdapter() instanceof StickyHeaderAdapter) {
            StickyHeaderAdapter adapter = (StickyHeaderAdapter) parent.getAdapter();
            long key = adapter.getHeaderId(position);

            if (headerCache.get(key) != null) {
                return headerCache.get(key);
            } else {
                RecyclerView.ViewHolder holder = adapter.onCreateHeaderViewHolder(parent);
                View header = holder.itemView;

                //noinspection unchecked
                adapter.onBindHeaderViewHolder(holder, position);

                int widthSpec = View.MeasureSpec.makeMeasureSpec(parent.getMeasuredWidth(), View.MeasureSpec.EXACTLY);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getMeasuredHeight(), View.MeasureSpec.UNSPECIFIED);

                int childWidth = ViewGroup.getChildMeasureSpec(
                        widthSpec,
                        parent.getPaddingLeft() + parent.getPaddingRight(),
                        header.getLayoutParams().width
                );
                int childHeight = ViewGroup.getChildMeasureSpec(
                        heightSpec,
                        parent.getPaddingTop() + parent.getPaddingBottom(),
                        header.getLayoutParams().height
                );

                header.measure(childWidth, childHeight);
                header.layout(0, 0, header.getMeasuredWidth(), header.getMeasuredHeight());

                headerCache.put(key, holder);

                return holder;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        if (parent.getAdapter() instanceof StickyHeaderAdapter) {
            StickyHeaderAdapter adapter = (StickyHeaderAdapter) parent.getAdapter();
            long previousHeaderId = -1;
            for (int layoutPos = 0, count = parent.getChildCount(); layoutPos < count; ++layoutPos) {
                View child = parent.getChildAt(layoutPos);
                int adapterPos = parent.getChildAdapterPosition(child);

                if (adapterPos != RecyclerView.NO_POSITION && hasHeader(adapter, adapterPos)) {
                    long headerId = adapter.getHeaderId(adapterPos);

                    if (headerId != previousHeaderId) {
                        previousHeaderId = headerId;
                        //Suppressed, because view can't be null here
                        //noinspection ConstantConditions
                        View header = getHeader(parent, adapterPos).itemView;
                        canvas.save();

                        int left = 0;
                        int top = getHeaderTop(parent, adapter, child, adapterPos, layoutPos);
                        canvas.translate(left, top);

                        header.setTranslationX(left);
                        header.setTranslationY(top);
                        header.draw(canvas);
                        canvas.restore();
                    }
                }
            }
        } else {
            if (parent.getAdapter() == null) {
                Log.w("StickyHeader", "Sticky headers disabled, because Adapter is null");
            } else {
                Log.w("StickyHeader", "Sticky headers disabled, because Adapter isn't implements StickyHeaderAdapter");
            }
        }
    }

    private int getHeaderTop(@NonNull RecyclerView parent, @NonNull StickyHeaderAdapter adapter, View child, int adapterPos, int layoutPos) {
        int top = ((int) child.getY());
        if (layoutPos == 0) {
            long currentId = adapter.getHeaderId(adapterPos);
            // find next view with header and compute the offscreen push if needed
            for (int i = 1, count = parent.getChildCount(); i < count; ++i) {
                int adapterPosHere = parent.getChildAdapterPosition(parent.getChildAt(i));
                if (adapterPosHere != RecyclerView.NO_POSITION) {
                    long nextId = adapter.getHeaderId(adapterPosHere);
                    if (nextId != currentId) {
                        View next = parent.getChildAt(i);
                        int offset = ((int) next.getY()) - (getHeader(parent, adapterPosHere).itemView.getHeight());
                        if (offset < 0) {
                            return offset;
                        } else {
                            break;
                        }
                    }
                }
            }
            top = Math.max(0, top);
        }
        return top;
    }
}
